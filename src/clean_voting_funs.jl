"""
Reads in a dataset where each observation is a bill. The dataset has 
information on whether or not a bill passed, the day of the vote, 
and how many votes it needed to pass. 
"""
function read_bills(billsfile)
	# Read in the file of bills
	billsfile_full = joinpath("data/bradrobinson-us-senate-voting-records/", billsfile)
	bills = CSV.File(billsfile_full) |> DataFrame 

	# Add a variable for the senate session, for merging
	session_file = replace(splitext(billsfile)[1], "bills" => "")
	
	# The first column is just a row counter. We don't need it
	bills = @pipe bills |>
			select!(_, Not(1)) |>
			rename!(_, "title" => "bill_name") |>
			insertcols!(_, "session_file" => session_file)

	# Use the `vote_result` column to get information on whether a bill needed 
	# 2/3 or 3/5 of votes to pass
	function get_pass_requirement(vote_result)
		if occursin("2/3 majority required", vote_result)
			return "two_thirds"
		elseif occursin("3/5 majority required", vote_result)
			return "three_fifths"
		else
			return "one_half"
		end
	end	

	# Clean the vote date, notice the string replacement
	# getting rid of excess white space
	function clean_vote_date(date_str)
		t = replace(date_str, r" +" => " ")
		# Check the docs for Dates here to understand this
		# kind of unreadable syntax.
		Dates.Date.(t, "U d, Y, I:M p") 
	end

	# Returns the votes, yes or no, of a bill by looking at the complicated `vote_result`
	# string.
	function get_yes_no(x)
		x = replace(x, ", 2/3 majority required" => "")
		x = replace(x, ", 3/5 majority required" => "")
		x = replace(x, ", Vice President voted Yea" => "")

		for_against_str = match(r"\(.*\)", x)
		if !isnothing(for_against_str)
			for_against = replace(for_against_str.match, r"\(|\)" => "")
			for_against_vec = split(for_against, "-")
			return (yes = parse(Int, for_against_vec[1]), no = parse(Int, for_against_vec[2]))
		else
			return (yes = missing, no = missing)
		end
	end

	bills = @pipe bills |> 
		transform!(_, "vote_result" => ByRow(get_pass_requirement) => "pass_requirement") |>
		transform!(_, "vote_date" => ByRow(clean_vote_date) => "date") |>
		transform!(_, "date" => ByRow(year) => "year") |> 
		sort!(_, ["bill_name", "date"])

	# Get the last vote for every bill. 
	# Sometimes bills are voted on multiple times, but we just want the last vote
	# in any senate session.
	yesno_nt = get_yes_no.(bills.vote_result)
	bills.yes_votes = getproperty.(yesno_nt, :yes)
	bills.no_votes = getproperty.(yesno_nt, :no)

	bills = combine(groupby(bills, "bill_name")) do b
		b[nrow(b), :]
	end

	return bills
end

"""
Produces the datasets of votes

The raw dataset has many columns, one for every vote, and 
100 rows, one for every senator. Each cell is whether or not 
a senator voted for a bill or not. 

Returns a dataset where each observation is a senator-vote. 
"""
function read_votes(votesfile)
	# Read in the data set
	votesfile_full = joinpath("data/bradrobinson-us-senate-voting-records/", votesfile)	
	votes = CSV.File(votesfile_full) |> DataFrame

	# Get the senate session by getting rid of the extension
	session_file = replace(splitext(votesfile)[1], "senate" => "")
	
	# Get the party demographics of that senate session
	n_dem = sum(t -> t == "D", votes.party)
	n_rep = sum(t -> t == "R", votes.party)
	n_ind = sum(t -> (t != "D") & (t != "R"), votes.party)

	# A helper function to generate a string variable for 
	# how a senator voted (or if they were absent)
	function replace_vote(x)
		if x == 0 
			return "no"
		elseif x == 1 
			return "yes"
		else
			return "absent"
		end
	end	

	# Perform the stack. 
	# Drop the first column, which is just a column name. 
	# The `stack` command automatically creates the variable
	# `value`, which we apply the `replace_vote` function 
	# to. 
	votes_long = @pipe votes |>
		select(_, Not(1)) |>
		stack(_, Not(["party", "name"]), variable_name = "bill_name") |>
		transform!(_, "value" => ByRow(replace_vote) => "how_voted") |>
		select!(_, Not("value")) |>
		insertcols!(_, "session_file" => session_file)
	return votes_long
end


"""
Produces a data set where each observation is a bill. We have how many 
Ds, Rs, and Is voted for a bill etc. and importantly, the population 
voting for or against a bill and whether or not the public was overruled
in that vote. 

* Raw data source: `https://data.world/bradrobinson/us-senate-voting-records`
* Files used: `"data/bradrobinson-us-senate-voting-records/*"`
* Dataset produced: `vote_level`
"""
function clean_voting!(D)
	# Unpack the datasets needed for this cleaning
	@unpack all_senators, states_long = D

	# Get all the filenames downloaded
	files = readdir("data/bradrobinson-us-senate-voting-records/")

	# Get the datasets that are bill-level
	bills_files = filter(t -> startswith(t, "bill"), files) |> sort!

	# For some reason there is bills for the 101_2 senate session but
	# no matching data-set for how senators voted. Not sure what 
	# to do here other than pretend this part doesn't exist. 
	#
	# TODO: explore better solutions to this. 
	bills_files = filter(t -> t != "bills102_2.csv", bills_files)

	# Get the datasets at the vote level.
	votes_files = filter(t -> startswith(t, "senate"), files) |> sort!

	# Apply the `read_bills` function to every file and vertically
	# concatenate the data frame
	bills = mapreduce(read_bills, vcat, bills_files)

	# Apply the `read_votes` function to every file and vertically
	# concatenate the data frames. 
	votes = mapreduce(read_votes, vcat, votes_files)

	# Perform some manual fixes of senator's names for joining with 
	# the dataset that has every senator's name (and state). 
	votes.name = replace!(votes.name, 
		"Brock Adams" => "Brockman Adams", 
		"Sherrod  Brown" => "Sherrod Brown",
		"Dan Coats" => "Daniel Coats",
		"Bill Bradley" => "William Bradley",
		"E. Nelson" => "Ben Nelson",
		"A. Allard" => "Wayne Allard",
		"Bob Casey" => "Robert Casey",
		"Ben Cardin" => "Benjamin Cardin",
		"Rudy Boschwitz" => "Rudolph Boschwitz",
		"Christopher Coons" => "Chris Coons",
		"Alfonse D'Amato" => "Alfonse D’Amato",
		"Hank Brown" => "George Brown",
		"Jim Talent" => "James Talent",
		"Tom Coburn" => "Thomas Coburn",
		"Jim Inhofe" => "James Inhofe",
		"Sam Brownback" => "Samuel Brownback",
		"Max Cleland" => "J. Cleland",
		"Mike DeWine" => "Michael DeWine",
		"Lauch Faircloth" => "Duncan Faircloth",
		"Bob Dole" => "Robert Dole",
		"Mike Crapo" => "Michael Crapo",
		"Bill Frist" => "William Frist",
		"Slade Gorton" => "T. Gorton",
		"Chuck Grassley" => "Charles Grassley",
		"Chuck Hagel" => "Charles Hagel",
		"Tom Harkin" => "Thomas Harkin",
		"Herb Kohl" => "Herbert Kohl",
		"Jack Reed" => "John Reed",
		"Jay Rockefeller" => "John Rockefeller",
		"Rick Santorum" => "Richard Santorum",
		"Jeff Sessions" => "Jefferson Sessions",
		"Bob Smith" => "Robert Smith",
		"Strom Thurmond" => "J. Thurmond",
		"Dave Durenberger" => "David Durenberger",
		"Jake Garn" => "Edwin Garn",
		"John Heinz" => "Henry Heinz",
		"J. Johnston" => "John Johnston",
		"Sam Nunn" => "Samuel Nunn",
		"Bob Packwood" => "Robert Packwood",
		"Terry Sanford" => "James Sanford",
		"Jim Sasser" => "James Sasser",
		"Steve Symms" => "Steven Symms",
		"Bob Krueger" => "Robert Krueger",
		"Carol Moseley-Braun" => "Carol Moseley Braun",
		"James DeMint" => "Jim DeMint",
		"Russ Feingold" => "Russell Feingold",
		"Johnny Isakson" => "John Isakson",
		"Joe Lieberman" => "Joseph Lieberman",
		"James Webb" => "Jim Webb",
		"Al Franken" => "Alan Franken",
		"Rob Portman" => "Robert Portman",
		"Pat Toomey" => "Patrick Toomey",
		"Bernie Sanders" => "Bernard Sanders",
		"Timothy Scott" => "Tim Scott",
		"James  Risch" => "James Risch",
		"Mike Enzi" => "Michael Enzi",
		"Ben Sasse" => "Benjamin Sasse",
		"Thomas Tillis" => "Thom Tillis"
		)

	# Join the votes and the bills files, so that at the level of 
	# senate-vote we also have information on the bill itself. 
	votes_bills = leftjoin(
		votes, 
		bills, 
		on = ["bill_name", "session_file"], 
		indicator = "_merge_bills"
	)

	# Check that each senator voted exactly once for every vote
	# This is important when debugging bad matches for 
	# senator's names. 
	function check_one_vote(df)

		bad_obs = nonunique(df, ["name", "bill_name", "session_file"])
		if any(bad_obs)
			print(df[bad_obs, :])
			error("Some senators vote more than once on a given bill")
		end

		return nothing
	end

	# Join the senator-vote level data set with senators. 
	# Unfortunately the senator-vote level data set doesn't include
	# which state the senator is from. So we have to merge 
	# with the senators data which includes the state they are 
	# from.
	votes_bills_legislators = leftjoin(
		votes_bills, 
		select(all_senators, Not("party")), 
		on = "name", 
		indicator = "_merge_senators"
	)

	# Join this data set with states, which includes the population
	# for each state for each year. 
	votes_bills_legislators_states = leftjoin(
		votes_bills_legislators, 
		states_long, 
		on = ["state", "year"], 
		indicator = "_merge_states"
	)

	# Define helper functions in preparation for collapse
	# How many Ds, Rs, and Is vote in any direction
	function partyvote(p, v, party, value)
		sum((p .== party) .& (v .== value))
	end

	# Write a function for the complicated collapse 
	# when we get data at the bill level. 
	function combine_fun(sdf)
		p = sdf.party
		v = sdf.how_voted
		pop = sdf.population ./ 2

		# variables that don't change across senator
		bill_vars = [
			"bill_name",
			"session_file", 
			"vote_result",
			"pass_requirement",
			"date",
			"year",
			"yes_votes",
			"no_votes"
			]

		# Make a row of variables that dont change across senator
		bills_row = sdf[1, bill_vars]

		# Make a named tuple of all the important statistics at 
		# the bill level we want. 
		new_nt = (
			num_yes = sum(v .== "yes"),
			num_no = sum(v .== "no"),
			num_absent = sum(v .== "absent"),

			num_D = sum(p .== "D"),
			num_R = sum(p .== "R"),
			num_I = sum(p .== "I"),

			num_D_yes = partyvote(p, v, "D", "yes"),
			num_R_yes = partyvote(p, v, "R", "yes"),
			num_I_yes = partyvote(p, v, "I", "yes"),
		
			num_D_no = partyvote(p, v, "D", "no"),
			num_R_no = partyvote(p, v, "R", "no"),
			num_I_no = partyvote(p, v, "I", "no"),
		
			num_D_absent = partyvote(p, v, "D", "absent"),
			num_R_absent = partyvote(p, v, "R", "absent"),
			num_I_absent = partyvote(p, v, "I", "absent"),
		
			total_pop = sum(pop),
			pop_yes = sum(pop[v .== "yes"]),
			pop_no = sum(pop[v .== "no"]),
			pop_absent = sum(pop[v .== "absent"]),

			pop_D = sum(pop[p .== "D"]),
			pop_R = sum(pop[p .== "R"]),
			pop_I = sum(pop[p .== "I"])
		)

		# Merge the row of bill-level data with the 
		# named tuple we just created above. 
		return merge(bills_row, new_nt)
	end

	# Perform the collapse using the function above to get 
	# a data-set where each observation is a bill. 
	#
	# Then we omit observations about articles of impeachment. 
	#
	# And omit observations that needed 3/5's of senators to pass. 
	#
	# TODO: explore if this actually deletes all filibuster votes
	# which may also be interesting to explore. 
	vote_level = @pipe votes_bills_legislators_states |>
		groupby(_, ["bill_name", "session_file"]) |>
		combine(combine_fun, _) |>
		filter("vote_result" => (t -> !occursin("Guilty", t)), _) |>
		filter("pass_requirement" => (t -> t == "one_half"), _)

	# Define whether or not the vote on a bill was where
	# the public was overruled. 
	vote_level = let vote_level = vote_level
		@unpack num_yes, num_no, pop_yes, pop_no = vote_level

		pop_yes_senate_no = @. (pop_yes > pop_no) & (num_yes < num_no)
		pop_no_senate_yes = @. (pop_yes < pop_no) & (num_yes > num_no)
		pop_overruled = @. pop_yes_senate_no | pop_no_senate_yes
		approved = @. num_yes > num_no

		@pack! vote_level = pop_yes_senate_no, pop_no_senate_yes, pop_overruled, approved

		vote_level
	end

	@pack! D = vote_level, votes_bills_legislators_states
end