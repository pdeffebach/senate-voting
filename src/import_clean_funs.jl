"""
Produces datasets of senators and representatives in the US congress.


* Raw data source: `https://github.com/unitedstates/congress-legislators`
* Files imported: 
  * `"data/legislators/legislators-historical.csv"`
  * `"data/legislators/legislators-current.csv"`
* Data-sets produced
  * `all_senators`
  * `current_senate`
  * `current_house`
"""
function clean_legislators!(D)
	# Import the historical dataset, i.e. not in congress now.
	historical = CSV.File("data/legislators/legislators-historical.csv") |> DataFrame
	
	# Keep only historical legislators who could plausibly have 
	# been in congress in 1989. 
	#
	# Then keep only senators (the data set includes both)
	#
	# Then keep just columns of interest. 
	keepvars = ["last_name", "first_name", "birthday", "gender", "state", "party"]
	
	historical_filtered = @pipe historical |>
		filter("birthday" => t -> !ismissing(t) && t > Date(1890, 01, 01), _) |>
		filter("type" => t -> t == "sen", _) |>
		select(_, keepvars)
	
	# Import current legislators
	current = CSV.File("data/legislators/legislators-current.csv") |> DataFrame
	
	# Make a dataset for current senators
	current_senate = @pipe current |>
		filter("type" => t -> t == "sen", _) |>
		select(_, keepvars)
	
	# Make a dataset for current members of the house
	current_house = @pipe current |>
		filter("type" => t -> t == "rep", _) |>
		select(_, keepvars)
	
	# Create a dataset of all senators who who could be in the senate 
	# between 1989 and 2016. 
	all_senators = vcat(historical_filtered, current_senate)
	
	# Clean the senators data. 
	# 
	# First, make a "full name" that is just the first and last names pasted together.
	#
	# Then we sort by last name and first name
	#
	# Then we filter our Albert Gore Sr. He was not in the senate in 1989 but due to 
	# his birthdate is in the historical data set he is in the data set. Do not
	# confuse him for his more famous son, Albert Gore Jr. Both were senators from
	# Tennessee. Having two Albert Gores in the  data set messes with merging to votes 
	# later on.
	all_senators = @pipe all_senators |>
		transform(_, ["last_name", "first_name"] => ByRow((l, f) -> string(f, " ", l)) => "name") |>
		sort!(_, ["last_name", "first_name"]) |>
		filter(_) do row
			(row.first_name == "Albert" && 
			 row.last_name == "Gore" && 
			 row.birthday == Date("1907-12-26")) == false
		end

	@pack! D = all_senators, current_senate, current_house
end

"""
Produces a balanced panel of yearly state population from 1989 to present.

* Raw data source: St. Louis Fed (using point and click so no url)
* File imported: `"data/State_Populations_txt/State_Populations_Annual.txt"`
* Dataset produced: `states_long` 
"""
function clean_states!(D)
	# Read in the dataset
	# Currently in "wide" format. Each row is a year and each 
	# column is a state. More than 50 columns because data includes
	# Virgin Islands, DC, etc. 
	states = CSV.File("data/State_Populations_txt/State_Populations_Annual.txt") |> DataFrame
	
	# Force a the vector to not be a `SentinalArrays` type of vector
	# see https://github.com/JuliaData/DataFrames.jl/issues/2326
	# But should be fixed on 1.5 and backported. 
	for (n, v) in pairs(eachcol(states))
		states[:, n] = [i for i in v]
	end
	
	# Go from wide to long format.
	# The first `transform` command happens because when 
	# we go to "long" format, observations are `"ORPOP"` etc. 
	# and we just need the `"OR"`. 
	states_long = @pipe states |>
		rename(_, "DATE" => "date") |>
		filter("date" => t -> t >= Date(1989, 01, 01), _) |>
		dropmissing(_) |>
		stack(_, Not(1); variable_name = "state", value_name = "population") |>
		transform(_, "state" => ByRow(t -> replace(String(t), "POP" => "")) => "state") |>
		transform(_, "date" => ByRow(year) => "year") |>
		select(_, Not("date"))

	@pack! D = states_long
end

"""
Produces committee assignments data for 2019

* Raw data source: `https://www.senate.gov/committees/` (copied and pasted)
* Files imported: "data/committees.tsv"
* Dataset produced: `states_committees`
* Uses dataset `states_long`
"""
function clean_committees!(D)
	# Unpack the `states_long` dataset in `D`.
	@unpack states_long = D

	# Read the committees dataset
	committees = CSV.File("data/committees.tsv") |> DataFrame
		
	# use a Regex to get the state of chair. observations
	# look like `"(R-SC)"`. Use Regex101.com to double check this
	# regular expression.
	committees = @pipe committees |>
		transform(_, "chairperson" => ByRow(s -> match(r"\(R-(.*)\)", s)[1]) => "state")
	
	# Match the committee chairs to the populations of 
	# each state in 2019. States without committees get `missing` after the join
	# and we use this to know whether or not a state has a committee.
	states_committees = @pipe states_long |>
		filter(row -> row.year == 2019, _) |>
		leftjoin(_, committees, on = "state") |>
		transform(_, "Committee" => ByRow(t -> ismissing(t) == false) => "has_committee") 

	@pack! D = states_committees
end

"""
Produces racial demographics by state in 2020

* Raw data source: `https://www.kff.org/other/state-indicator/distribution-by-raceethnicity/`
* Files imported: 
  * `"data/race_states/race_states.csv"`
  * `"data/race_states/states_crosswalk.csv"`. This is just a crosswalk that I think
    I got from copying and pasting the Wikipedia for US states. Its just "Alabama" to "AL"
    etc. 
"""
function clean_race_states!(D)
	# Read in the sttes
	# Count `<.01` as a missing to avoid annoying parsing issues
	race_states = CSV.File("data/race_states/race_states.csv", missingstrings = ["N/A", "<.01"]) |> DataFrame
	
	# Read in the cross walk
	crosswalk = CSV.File("data/race_states/states_crosswalk.csv") |> DataFrame
	
	# Fill in missing values as `0`, coming from `N/A` and `<.01`
	for var in names(race_states, Between("White", "Two Or More Races"))
		race_states[:, var] = coalesce.(race_states[:, var], 0)
	end
	
	# Join the race data with the crosswalk so we have AL, CA etc. 
	race_states = @pipe race_states |>
		leftjoin(_, crosswalk, on = "Location" => "State") |>
		select(_, Not(["Location", "Abbrev"])) |>
		rename(_, "Code" => "state") |>
		filter(r -> !ismissing(r.state) && r.state != "DC", _)

	# Change from wide to long format
	race_states = @pipe race_states |>
		select(_, Not("Total")) |>
		stack(_, Not("state"); variable_name = "race", value_name = "pop")
		
	@pack! D = race_states
end






