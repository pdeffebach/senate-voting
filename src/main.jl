# ### Change directory to `SenateVoting`
HOMEDIR = joinpath(@__DIR__, "..")
cd(HOMEDIR)

# ### Activate the current environment
import Pkg
Pkg.activate(".")

# ### Download necessary packages
Pkg.instantiate()

#  ### Import modules
using Pipe: @pipe
using DataFrames, CSV, Dates, Statistics, UnPack, PrettyTables, StatsBase, Plots, Tar

# ### Download data
include(joinpath(HOMEDIR, "src", "data_download.jl"))

# ### Define functions for cleaning
include(joinpath(HOMEDIR, "src", "import_clean_funs.jl"))
include(joinpath(HOMEDIR, "src", "clean_voting_funs.jl"))

# ### Define functions for the analysis
include(joinpath(HOMEDIR, "src", "analysis_funs.jl"))

# ### Clean the data
# `D` is a global dict that stores all the data 
# sets used in this project. Each function adds 
# new data to `D`, consuequently all functions 
# end with a `!`. 
#
# The order that the functions are executed in matters, 
# since the construction of some data sets depend on 
# other data existing already.
println("Beginning cleaning")

D = Dict{Symbol, DataFrame}()
clean_legislators!(D)
clean_states!(D)
clean_committees!(D)
clean_race_states!(D)
clean_voting!(D)

# ### Analyze the data
# Each function uses data sets defined in `D`. 
# Some produce graphs, some produce tables, 
# and others produce named tuples.
println("Beginning analysis")

# Initialize a dictionary in which to
# hold results
results = Dict()

# Magnitude of representation in the Senate across States
results[:senators_per_resident] = senators_per_resident(D)

# Race and Representation in the Senate
results[:senators_per_resident_race] = senators_per_resident_race(D)

# ...in 2016 60% of senators were from states with below median population Republican,
# compared to 48% of senators in states with above median population.
results[:pop_party_median] = pop_party_median(D)

# Population and Voting Power Discrepancy
results[:compare_pop_votes] = compare_pop_votes(D, "D")

# This function produces 3 graphs
t = population_overruled_time(D)

# Percentage of votes where population is overruled across time
results[:pop_overruled_time] = t.pop_overruled_time

# Percentage of votes where population is overruled across time, Motions to Table
# recategorized
results[:pop_overruled_time_table] = t.pop_overruled_time_table

# Percentage of Senate Republican and Percentage of “undemocratic votes”
results[:scatter_R_overruled]  = t.scatter_R_overruled

# Average outcomes across item types
results[:which_pop_overruled] = which_pop_overruled(D)

# Nonetheless, of bills that pass with 60 votes or fewer, 
# meaning bills that could have been filibustered but for some reason were 
# not, the population is overruled 33% of the time.
results[:mean_bill_less_60] = mean_bill_less_60(D)

# I show that the states with
# committee heads in our current Republican-controlled Senate have 
# populations on average 77% the size of the population of 
# the average state.
results[:committee_state_pop_frac] = committee_state_pop_frac(D)

# In time periods where Democrats are in control of the Senate, the 
# average number of votes overruled is only 7%, compared to 21% 
# when Republicans are in charge.
results[:overruled_by_party] = overruled_by_party(D)

# Then I examined the top 10 such items that passed the Senate but when
# senators representing the majority of the U.S. population opposed, 
# giving me the top 10 most undemocratic “Population no, Senate yes” 
# votes. I performed the same operation on “Population yes, Senate no” 
# votes as well.
results[:most_unequal] = most_unequal(D)

# In 2020, in states with two Democratic senators, 15% of House seats 
# are held by Republicans. Conversely, in states with two Republican 
# senators, 20% of House seats are held by Democrats.
results[:percent_opposite_party] = percent_opposite_party(D)

# ### Specific numbers in the text
# Numbers in the text that don't need calculations but
# are referenced nonetheless. 

# Votes have, on average, 97 senators voting
# either “Yea” or “Nay” and 3 senators absent
mean(==("absent"), D[:votes_bills_legislators_states].how_voted)

# Over the period 1989-2016, 14% of all votes which required a majority vote to
# pass fell into one of the two categories above.
mean(D[:vote_level].pop_overruled)

println("Successful exit")