# Senate Voting

A project exploring the gaps between the number of senators voting and the population they represent. 

To run this code, 

1. Clone this repository
2. Email me requesting a private download link for the data used in this project.
3. Run `src/main.jl`. When prompted, paste the download link into the terminal. 