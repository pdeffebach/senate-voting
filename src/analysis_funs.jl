"""
Produces a bar plot of senators per voter in 2019. Small states have
large values. All values are normalized relative to California, the
state with the largest population.
"""
function senators_per_resident(D)
	@unpack states_long = D

	# 2019 is the latest year we have data on. Even though
	# it's outside the sample period for voting data.
	#
	# Make an inverse population variables.
	# Worrying about how states have 2 senators doesn't matter
	# since everything is relative to California.
	spv = @pipe states_long |>
		filter(r -> r.year == 2019, _) |>
		filter(r -> r.state != "DC", _) |>
		select(_, "state", "population") |>
		transform(_, "population" => ByRow(inv) => "senators_per_resident") |>
		transform(_, "senators_per_resident" => (x -> x / minimum(x)) => "senators_per_resident") |>
		sort!(_, "senators_per_resident")

	p = bar(
		spv.state,
		spv.senators_per_resident,
		color = "black",
		fill = "black",
		legend = :none,
		grid = :none,
		yaxis = "Senators per voter relative to California"
	)

	savefig(p, "out/senators_per_resident.png")

	return p
end

"""
Votes per senator by race, rather than by state. The variable
of interest is a weighted average across all members of a race
of their representation.
"""
function senators_per_resident_race(D)
	@unpack states_long, race_states = D

	# Filter the year we want and omit DC.
	# Filtering out DC is a big omission. TODO:
	# understand how this changes results.
	s = @pipe states_long |>
		filter(r -> r.year == 2019, _) |>
		filter(r -> r.state != "DC", _) |>
		select(_, "state", "population") |>
		transform(_, "population" => ByRow(inv) => "inv_pop")

	# Merge the total population data onto the race data, then collapse
	# to get the average senators per resident by race
	out = @pipe race_states |>
		leftjoin(_, s; on = "state") |>
		transform(_, ["pop", "population"] => ByRow(*) => "total_pop") |>
		groupby(_, "race") |>
		combine(_) do sdf
			(senators_per_resident = mean(sdf.inv_pop, weights(sdf.total_pop)), )
		end

	# Normalize so that all are in terms of "White"
	whiterow = findfirst(==("White"), out.race)
	out.senators_per_resident = out.senators_per_resident ./ out.senators_per_resident[whiterow]

	# Now we make the table nice for printing
	sort!(out, "senators_per_resident")
	rename!(out,
		"senators_per_resident" => "Senators per resident relative to Whites",
		"race" => "Race"
	)

	# Use pretty_table to print the output.
	open("out/senators_per_resident_race.tex", "w") do f
		pretty_table(
			f,
			out,
			backend = :latex,
			nosubheader = true,
			wrap_table = false,
			formatters = ft_printf("%0.1f"))
	end

	return out
end

"""
Average party membership above and below median population
"""
function pop_party_median(D)
	@unpack votes_bills_legislators_states = D

	# Get a snapshot of the senate in the most recent vote
	# this assumes that the data set is sorted properly,
	# but we check this below.
	d =	@pipe votes_bills_legislators_states |>
		filter("year" => ==(2016), _) |>
		_[end-99:end, :]

	# make sure all states represented
	gd = groupby(d, "state")
	@assert length(gd) == 50 && all(sdf -> nrow(sdf) == 2, gd)

	median_pop = median(d.population)

	return (below_median_R_mean = mean(t -> t == "R", d[d.population .< median_pop, "party"]),
	above_median_R_mean = mean(t -> t == "R", d[d.population .> median_pop, "party"]))
end

"""
Graph of the fraction of senate seats a party has across
time compared with the fraction of the US population the
party represents.

`party` can be wither `"D"`, `"R"`, or `"I"`
"""
function compare_pop_votes(D, party)
	@unpack vote_level = D
	# Turn the absolute values into fractions for both the
	# population and the number of Senators
	d = @pipe vote_level |>
	unique(_, ["date"]) |>
	transform!(_,
		["pop_$(party)", "total_pop"] => ByRow(/) => "frac_pop_$(party)",
		"num_$(party)" => ByRow(t -> t / 100) => "frac_num_$(party)",
	) |>
	sort!(_, "date")

	# Make sure to multiply by 100 for percentages
	p = plot(
		year.(d.date),
		[d[!, "frac_pop_$(party)"] d[!, "frac_num_$(party)"]] .* 100,
		label = ["% pop. represented by $(party)" "% of Senate $(party)"],
		color = "black",
		line = [:solid :dash],
		xaxis = "Year",
		grid = :none,
		ylim = (30, 70),
		legend = :outerbottom
	)

	hline!(p, [50], color = "black", line = :dot, label = :none)

	savefig(p, "out/$(party)_inequality.png")

	return p
end

"""
Plots the proportion of votes where the population is overruled, across time.

Creates two graphs, one *without* Motions to Table being classified as
"Population yes, Senate no" and one *with* the reclassification performed.

Also make a graph showing the relationship between Republican control of the Senate
and the percentage of undemocratic votes.
"""
function population_overruled_time(D)
	@unpack vote_level = D

	# Things can change across years due to senators leaving
	# office etc. So you take mean.
	#
	# This is without motions to table reclassified
	#
	# `y` stands for "year level"
	y = @pipe vote_level |>
		groupby(_, "year") |>
		combine(_, ["pop_yes_senate_no",
			        "pop_no_senate_yes",
			        "pop_overruled",
			        "num_R",
			        "num_D"] .=> mean) |>
		sort!(_, "year")

	# Make the plot without motions to table reclassified.
	pop_overruled_time = plot(
		y.year,
		[y.pop_overruled_mean y.pop_yes_senate_no_mean y.pop_no_senate_yes_mean] .* 100,
		label = ["Population overruled" "Population yes, Senate no" "Population no, Senate yes"],
		color = "black",
		line = [:solid :dash :dot],
		grid = :none,
		legend = :outerbottom,
		yaxis = "Percentage of votes"
	)

	savefig(pop_overruled_time, "out/population_overruled_time.png")

	# Use the same year-level data set to show the correlation between
	# R control of the senate and the proportion of undemocratic votes.
	scatter_R_overruled = scatter(
		y.num_R_mean,
		y.pop_overruled_mean .* 100,
		color = "black",
		xlabel = "% of Senate R",
		ylabel = "% of votes pop. overruled",
		label = :none,
		grid = :none
	)

	savefig(scatter_R_overruled, "out/scatter_R_overruled.png")

	# Define a function to reverse the classification of
	# undemocratic votes for motions to table.
	#
	# If a bill name has "Table" in it, we reverse.
	function reverse_pop(pop_yes_senate_no, pop_no_senate_yes, vote_result)
		t = occursin("Table", vote_result)
		if t == true && pop_yes_senate_no == true
			return (pop_yes_senate_no = false, pop_no_senate_yes = true)
		elseif t == true && pop_no_senate_yes == true
			return (pop_yes_senate_no = true, pop_no_senate_yes = false)
		else
			# In Julia 1.5 this will be less wordy
			return (pop_yes_senate_no = pop_yes_senate_no, pop_no_senate_yes = pop_no_senate_yes)
		end
	end

	# Copy the data frame because we will alter it
	d = copy(vote_level)

	# Apply the reversing function
	reverse_pop_result = reverse_pop.(
		d.pop_yes_senate_no,
		d.pop_no_senate_yes,
		d.vote_result)

	# Overwrite the data frame with the new results
	d.pop_yes_senate_no = getfield.(reverse_pop_result, :pop_yes_senate_no)
	d.pop_no_senate_yes = getfield.(reverse_pop_result, :pop_no_senate_yes)

	# Make a new year-level dataset
	# with the change
	y2 = @pipe d |>
		groupby(_, "year") |>
		combine(_, ["pop_yes_senate_no",
			        "pop_no_senate_yes",
			        "pop_overruled",
			        "num_R",
			        "num_D"] .=> mean) |>
		sort!(_, "year")

	pop_overruled_time_table = plot(
		y2.year,
		[y2.pop_overruled_mean y2.pop_yes_senate_no_mean y2.pop_no_senate_yes_mean] .* 100,
		label = ["Population overruled" "Population yes, Senate no (Approved motions to table included)" "Population no, Senate yes (Failed motions to table included)"],
		color = "black",
		line = [:solid :dash :dot],
		grid = :none,
		legend = :outerbottom,
		yaxis = "Percentage of votes"
	)

	savefig(pop_overruled_time_table, "out/population_overruled_time_motion_to_table.png")

	return(
		pop_overruled_time = pop_overruled_time,
		scatter_R_overruled = scatter_R_overruled,
		pop_overruled_time_table = pop_overruled_time_table
	)
end

"""
Creates a data frame showing which kinds of votes are ones where
the population is overruled, i.e Bills, Amendments, Nominations, etc.
"""
function which_pop_overruled(D)
	@unpack vote_level = D

	d = copy(vote_level)

	# Make a better vote type variable
	d."Vote Type" = map(d.vote_result) do t
		if occursin("Bill", t)
			"Bill"
		elseif occursin("Amendment", t)
			"Amendment"
		elseif occursin("Nomination", t)
			"Nomination"
		elseif occursin("Table", t)
			"Motion to Table"
		else
			"Other"
		end
	end

	# Combine to get the mean of undemocratedness and
	# other variables by type of vote.
	N = nrow(d)
	out = @pipe d |>
		groupby(_, "Vote Type") |>
		combine(_) do sdf
			DataFrame(
				"\\% Pop. overruled" => mean(sdf.pop_overruled) * 100,
				"\\% Pop. yes, Senate no" => mean(sdf.pop_yes_senate_no) * 100,
				"\\% Pop. no, Senate yes" => mean(sdf.pop_no_senate_yes) * 100,
				"\\% Pass" => mean(sdf.approved) * 100,
				"\\% of total" => (nrow(sdf) / N) * 100
			)
		end

	# Use pretty_table for printing to a file.
	open("out/which_pop_overruled.tex", "w") do f
		pretty_table(
			f,
			out,
			backend = :latex,
			nosubheader = true,
			wrap_table = false,
			formatters = ft_printf("%0.0f"))
	end

	return out
end

"""
Mean passage rate of bills (as opposed to amendments etc.)
with less than 60 votes for them, meaning veto is not an issue.
"""
function mean_bill_less_60(D)
	@unpack vote_level = D

	s = filter(r -> (occursin("Bill", r.vote_result)) & (r.num_yes <= 60), vote_level)
	(mean_pop_overruled_less_66 = mean(s.pop_overruled), )
end

"""
Average population of states with committee heads
"""
function committee_state_pop_frac(D)
	@unpack states_committees = D

	# Mean for the whole population
	m = mean(states_committees.population)

	# Mean for states with committees
	m_committees = mean(states_committees.population[states_committees.has_committee])

	(mean_pop_has_committee = m_committees / m, )
end

"""
Mean amount of undemcratic votes by which party is in control
"""
function overruled_by_party(D)
	@unpack vote_level = D

	D = filter(r -> r.num_D > r.num_R, vote_level)
	R = filter(r -> r.num_D < r.num_R, vote_level)

	(D_mean = mean(D.pop_overruled), R_mean = R_mean = mean(R.pop_overruled))
end

"""
The top 10 most unequal bills, meaning the largest gap between
senate vote and the proportion of US popualtion (among voting senators)
who voted for a bill.
"""
function most_unequal(D)
	@unpack vote_level = D

	# Get approved bills
	approved = filter(:approved => identity, vote_level)

	# Convert to fractions
	approved.frac_pop_yes = @. approved.pop_yes / (approved.pop_yes + approved.pop_no)
	approved.frac_senate_yes = @. approved.num_yes / (approved.num_yes + approved.num_no)

	# Only undemocratic outcomes
	approved = filter(r -> r.frac_pop_yes < .5, approved)

	approved.gap = approved.frac_senate_yes - approved.frac_pop_yes

	# Sort and take top 10
	approved = sort!(approved, "gap", rev = true)[1:10, :]

	# Get failed bills
	failed = filter("approved" => ==(false), vote_level)

	# Convert to fractions
	failed.frac_pop_yes = @. failed.pop_yes / (failed.pop_yes + failed.pop_no)
	failed.frac_senate_yes = @. failed.num_yes / (failed.num_yes + failed.num_no)

	# Only undemocratic outcomes
	failed = filter(r -> r.frac_pop_yes > .5, failed)

	failed.gap = failed.frac_pop_yes - failed.frac_senate_yes

	# Sort and take top 10
	failed = sort!(failed, "gap", rev = true)[1:10, :]

	return(most_unequal_approved = approved, most_unequal_failed = failed)
end

"""
For each state with 2 Democratic senators, calculate the percentage
of house seats that are Republican. Similarly, for each
state with 2 Republican senators, calculate the percentage of house seats
that are Democrat.
"""
function percent_opposite_party(D)
	@unpack current_senate, current_house = D

	# Figure out which states are both D, which
	# states are both R
	states_party = @pipe current_senate |>
	    groupby(_, "state") |>
	    combine(_) do sdf
	    	p = sdf.party
	    	if all(==("Democrat"), p)
	    		return (class = "All D",)
	    	elseif all(==("Republican"), p)
	    		return (class = "All R",)
	    	else
	    		return (class = "Mixed",)
	    	end
	    end

	# Make a variable for the opposite party
	# of a state. p_D, p_R, and p_R are
	# percentages of each party. We just want
	# to focus on the opposite one
	function get_opp(class, p_D, p_R, p_I)
		if class == "All D"
			return p_R
		elseif class == "All R"
			return p_D
		else
			return missing
		end
	end

	# Attach this state-level senate data to the house data set.
	# Then collapse at the state level to get percentage filled
	# by each party
	out = @pipe current_house |>
	    leftjoin(_, states_party; on = "state") |>
	    groupby(_, "state") |>
	    combine(_) do sdf
	    	p = sdf.party
	    	(
	    		percent_D = mean(==("Democrat"), p),
	    		percent_R = mean(==("Republican"), p),
	    		percent_I = mean(==("Independent"), p),
	    		class = first(sdf.class)
	    	)

	   end |> # Now do the "percentage of opposite party" function
	   transform(_,
	    	["class", "percent_D", "percent_R", "percent_I"] => ByRow(passmissing(get_opp)) => "percent_opp"
	   ) |> # Group by "2 D, 2 R, or Mxed". Should get `missing` for mixed
	   groupby(_, "class") |>
	   combine(_, "percent_opp" => mean, nrow)

	return out
end

