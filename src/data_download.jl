# Check if there is a data folder. If not, 
# then download the data from Dropbox using the link
# I provided. 
if isdir("data") == false
	welcome_string = 
"""
Welcome to Peter Deffebach's Senate Voting project, 
helping us understand the extent of countermajoritarian
outcomes in the Senate.

It is likely that this is your first time working in this
project since you have not yet downloaded the data required
for running the analysis. 

Due to concerns over data-protection, Peter only provides 
the link to share the data upon request. If you have 
received one of these links, please paste it in the prompt
below.
"""
	println(welcome_string)
	print("Paste url here:")
	dataurl = readline()
	Base.download(dataurl, "data.tar")
	Tar.extract("data.tar", "data")
	rm("data.tar")
	mkdir("out")
end