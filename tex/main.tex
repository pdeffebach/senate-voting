\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[dvipsnames]{xcolor}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathbbol}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{adjustbox}
\usepackage{float}
\usepackage{grffile}
\usepackage{minibox}
\usepackage{booktabs}
\usepackage{fouriernc}
\usepackage[T1]{fontenc}
\usepackage[parfill]{parskip}
\usepackage{lineno}
\geometry{margin = 1in}

\title{Inequality of Votes in the Senate}
\date{\today}
\author{Peter Deffebach}

\begin{document}
\maketitle

The senate is an undemocratic institution: it represents states, not people. This argument is as old as it is obvious, with Alexander Hamilton \href{https://avalon.law.yale.edu/18th_century/fed22.asp}{writing} in Federalist 22, published in 1989, that the Senate ``contradicts the fundamental maxim of republican government, which requires that the sense of the majority should prevail.''

Over the past 230 years, the US's democratic notions that ``the majority should prevail'' have strengthened.  On the other hand, urbanization and westward expansion have caused citizens of states with small populations to have a larger voice in the Senate than citizens in states with larger populations. 

The election of Donald Trump by a minority of the U.S. population and Senate Republicans' unrelenting obstinateness have reinvigorated public debate on the institution. Over the past several years, \href{https://www.theatlantic.com/ideas/archive/2019/01/heres-how-fix-senate/579172/}{more} and \href{https://www.vox.com/2018/10/13/17971340/the-senate-represents-states-not-people-constitution-kavanaugh-supreme-court}{more} commentators have made damning arguments against the Senate.  In this blog post, I quantify the yawning gap between the political power of citizens in large and small states and reveal the extent of the Senate's un-representativeness. I examine the way the Senate subverts ``one person one vote'' logic, the way the institution currently cements Republican political power, and reveal the frequency of undemocratic outcomes at the level of individual votes. 

\section{Inequality of senators per resident}

Since 1964, seats in the US House of Representatives have been required be apportioned according to the rule of ``one person one vote'', meaning all House districts must have roughly equal populations. It is, of course, impossible to apply the same standard to the Senate given that each state is alloted exactly two senators. 

New York Times columnist Jamelle Bouie aptly summarized the scale of this dimension of the Senate's un-democraticness in a 2019 \href{https://www.nytimes.com/2019/05/10/opinion/sunday/senate-democrats-trump.html}{article}, writing

\begin{quote}
In 1790, the largest state was Virginia, with 747,610 people, and the smallest was Delaware, with 59,094 people. Because of the Senate’s equal representation of states, Delawareans had more than 12 times the voting power of Virginians — a large disparity, but not a yawning one. Today, the largest state is California, with nearly 40 million residents, and the smallest is Wyoming, with just under 600,000 people, a disparity that gives a person in Wyoming 67 times the voting power of one in California.
\end{quote}

Figure \ref{senators_per_resident}, visualizes this logic. It shows the inverse of the population of each states, re-scaled so that California, the state with the largest population, has a value of 1. Figure \ref{senators_per_resident} shows, as Bouie writes, that a citizen of Wyoming has 67 times the representation in the senate as a citizen from California. 

\begin{figure}[H]
\centering
\caption{Magnitude of representation in the Senate across States}
\label{senators_per_resident}
\includegraphics[width = .7\textwidth]{../out/senators_per_resident.png}
\end{figure}

Inequalities are present not only at the state level. Due to the fact that small states are Whiter than large states, the structure of the Senate means that Black, Hispanic, and Asian populations have significantly less representation in the Senate relative to Whites. 

\begin{table}[H]
\centering
\caption{Race and Representation in the Senate}
\label{which_pop_overruled}
\begin{adjustbox}{width=1\textwidth}
\input{../out/senators_per_resident_race.tex}
\end{adjustbox}
\end{table}

\section{Undemocratic votes}

For the most part, empirical arguments about ``representative inequality'' in the Senate have followed the same template as Bouie's: a widening gulf in voting power between people in populous and sparse states. 

Among the ranks of these pieces is Jay Willis's \href{https://www.gq.com/story/the-case-for-abolishing-the-senate}{article} in GQ that we should abolish the Senate all together. In it, Willis goes beyond citizens-per-senator inequality and references specific votes in which the public was overruled. 

\begin{quote}
The 50 senators who voted to confirm the wildly-unpopular Brett Kavanaugh represent only 44 percent of the population; the 51 senators who passed a widely-reviled \$1.5 trillion tax cut for the wealthy, about the same.
\end{quote}

I wondered: how widespread is this phenomenon? How often is the will of the majority of the U.S. population overruled by senators representing states whose collective population is less than 50\% of nation's?

In 2018, GovTrack U.S. performed this \href{https://govtrackinsider.com/with-kavanaugh-vote-the-senate-reaches-a-historic-low-in-democratic-metric-dfb0f5fa7fa}{analysis} from the years 1901 to the present, finding that on average, items --- bills, amendments, nominations, and senate motions --- which passed the Senate were not routinely passed by a minority of U.S. senators until after the year 2016. However their analysis is lacking. 

First, it only shows measures that \emph{passed} in the Senate, not all votes. A vote to defeat an item may be just as consequential as a vote to pass an item. Because of the House of Representatives, all bills which become law are approved by congresspeople cumulatively representing a majority of the U.S. population, so we should be more concerned by counter-majoritarian outcomes from bills being blocked by the senate than those passing. 

Second, their analysis does not delve fully into the correlation between Republican control of the senate and votes where, in their terms, there is a ``large a discrepancy between votes and voters''. Third, and finally, GovTrack did not analyze which kinds of votes and topics were ones where the will of senators representing the majority of the U.S. population was overturned. It's impossible to appropriately assess the consequences of the Senate's undemocratic structure without this context.

To further explore these questions, I used senator-level voting data available \href{https://data.world/bradrobinson/us-senate-voting-records}{online} from 1989 to 2016, along with annual state-level population data from the St. Louis Fed, to show, for each Senate vote

\begin{enumerate}
\item Whether or not an item was passed by senators representing a minority of the population, which I call ``Population no, Senate yes''
\item Conversely, whether an item was blocked despite a majority of the population voting in favor: ``Population yes, Senate no''
\end{enumerate}

I define a vote as having an ``undemocratic'' outcome if it falls into one of the two categories above. Additionally, note that votes, not bills, are the units of observation; in addition to bills, my dataset includes amendments, nominations, motions to table, and more. 

Note that while I make a distinction between ``Population no, Senate yes'' and ``Population yes, Senate no'' in my graphs and data, in practice the line between the two forms of ``undemocratic votes'' is blurry.  The problem is``Motion to Table'' votes which effectively kill an amendment by ending debate on an amendment. The amendment dies in the Senate without a formal vote on the item itself. In these votes, the ``Yeas'' outnumber the ``Nays'', but the result is roughly equivalent to the item being up for a floor vote and defeated. Any interpretation of the relative frequency of ``Population no, Senate yes'' and ``Population yes, Senate no'' must bear this in mind. 

Additionally, votes are not necessarily the final verdict on a particular item. The same text of an amendment can be re-introduced in multiple bills. I cannot claim that ``Population yes, Senate no'' items were \emph{never} enacted by the U.S. Senate between their defeat and today, and I don't have the resources or expertise to track down the full history of proposed amendments and bills. Similarly, it is hard for me to determine whether or not a particular approved senate item was later overturned in new legislation. Finally, I cannot determine if the final language of a bill --- passed by both the House and Senate --- included the changes of a particular amendment. 

Finally, not all senators vote on every item. Votes have, on average, 97 senators voting either ``Yea'' or ``Nay'' and 3 senators absent. Consequently, the fraction of senators voting for or against a bill are not always of the form ``\emph{x} over one hundred''. When calculating the fraction of the U.S. population represented by these senators, the denominator is the fraction of total population of senators voting for or against a bill, not necessarily the total U.S. population. I sacrifice exactness for legibility below when I refer to senators ``representing the majority of the U.S. population''; in reality this means senators representing the majority of the population \emph{among states whose senators voted}. 

With these caveats in mind, let's explore undemocratic votes in the Senate. 

First, I analyze the general trend of the number of ``undemocratic votes'' across time in both categories. Over the period 1989-2016, 14\% of all votes which required a majority vote to pass fell into one of the two categories above. The frequency of these outcomes is plotted in Figure \ref{population_overruled_time}, below.

\begin{figure}[H]
\centering
\caption{Percentage of votes where population is overruled across time}
\label{population_overruled_time}
\includegraphics[width = .7\textwidth]{../out/population_overruled_time.png}
\end{figure}

As the solid line in Figure \ref{population_overruled_time} indicates, 2015 had the highest percentage of undemocratic votes in this time frame, topping out at 34\% in 2015. 

In line with my first caveat above about how Motions to Table are effectively the Senate saying ``No'' to an item. In Figure \ref{population_overruled_time_motion_to_table}, below, I re-categorized Motions to Table votes to better understand whether ``undemocratic votes'' function to stop progress or to add legistlation over the objections of senators representing the majority of the U.S. population. As Figure \ref{population_overruled_time_motion_to_table} shows, in most years in the sample, ``undemocratic votes'' to stop changes vastly outnumber ``undemocratic votes'' to introduce new changes. However, the two types of ``undemocratic votes'' happen with similar frequencies from the year 2005 onward.  

\begin{figure}[H]
\centering
\caption{Percentage of votes where population is overruled across time, Motions to Table recategorized}
\label{population_overruled_time_motion_to_table}
\includegraphics[width = .7\textwidth]{../out/population_overruled_time_motion_to_table.png}
\end{figure}

What kinds of items lead to the most undemocratic outcomes? Table \ref{which_pop_overruled} shows that Amendments and Motions to Table, which are used to kill amendments, are the most likely to be overruled. 17\% of the time the Senate chooses to kill an amendment via a Motion to Table, senators representing the majority of the U.S. population are in favor of that amendment. 

Bills, which include bills both originating in the Senate and House Resolutions, on the other hand, \emph{always} pass and are rarely overruled. This could be due to the fact that senators generally know how their compatriots will vote and choose to bring a vote to the floor accordingly. It is also possible that bills frequently die via voice votes, which are not recorded in the data set. The largest culprit for 100\% pass-rate of bills is, of course, the filibuster. If a bill cannot get a floor vote without the support of 60 or more senators, then all bills will surely pass. Unfortunately my data does not include information on filibusters and I cannot examine their role in minority rule in the Senate. 

Nonetheless, of bills that pass with 60 votes or fewer, meaning bills that could have been filibustered but for some reason were not, the population is overruled 33\% of the time. The vast majority of such bills are House resolutions, meaning Senate approval creates law. Senators representing the majority of the US population are unable to act as a check against these laws. 

\begin{table}[H]
\centering
\caption{Average outcomes across item types}
\label{which_pop_overruled}
\begin{adjustbox}{width=1\textwidth}
\input{../out/which_pop_overruled.tex}
\end{adjustbox}
\end{table}

As an economics student, I understand that this data is far from showing how the power of small states truly operates in the Senate. As mentioned above, because of Senate strategy, final votes on bills are nowhere close to representative of the true support of the bill's proposals. This is, of course, ignoring the role of committees in filtering out bills before they ever make it to the Senate floor. 

It is likely that ``undemocratic votes'' happen at every stage of the process. For example, using data on present day committee assignment available \href{https://www.senate.gov/committees/}{online}, I show that the states with committee heads in our current Republican-controlled Senate have populations on average 77\% the size of the population of the average state. 

\section{Partisanship of minority rule}

Smaller states elect Republican senators more frequently than larger states. In particular, in 2016 60\% of senators were from states with below median population Republican, compared to 48\% of senators in states with above median population. It comes as no surprise, then, that the share of ``undemocratic votes'' is higher when Republicans control the Senate. 

In time periods where Democrats are in control of the Senate, the average number of votes overruled is only 7\%, compared to 21\% when Republicans are in charge. Figure \ref{scatter_R_overruled} shows this pattern in detail. There is a strong correlation between the number of Republicans in the Senate and the fraction of undemocratic votes by year. 

\begin{figure}[H]
\centering
\caption{Percentage of Senate Republican and Percentage of ``undemocratic votes''}
\label{scatter_R_overruled}
\includegraphics[width = .7\textwidth]{../out/scatter_R_overruled.png}
\end{figure}

Focusing on partisanship can feel reductive because undemocratic votes are a function of population rather than party control, but it is worth emphasizing just how underrepresented voters in larger, Democratic states are. Figure \ref{D_inequality} shows that Democratic senators always have less power than they should based on the population they represent. While Democratic senators collectively represent over half the U.S. population for almost the entire sample, they are in control of the Senate less than half the time. 

If Senate votes were alloted according to population, as proposed by Eric W. Orts in The \href{https://www.theatlantic.com/ideas/archive/2019/01/heres-how-fix-senate/579172/}{Atlantic}, Democratic senators would have had on average six more votes than they did between the years 1989 and 2016. 

\begin{figure}[H]
\centering
\caption{Population and Voting Power Discrepancy}
\label{D_inequality}
\includegraphics[width = .7\textwidth]{../out/D_inequality.png}
\end{figure}

That there are only two senators per state can also raise issues of representation \emph{within} a state. For instance, if states with two Democratic senators have more Republican voters than states with two Republican senators have Democratic voters, then problem of the Senate over-representing small states is corrected somewhat: small red state senators also represent Republicans in hidden Blue states. 

Aside from the flaws in this argument --- it presupposes that a senator cannot represent someone of a different political party --- the reasoning has no empirical foundation. In 2020, in states with two Democratic senators, 15\% of House seats are held by Republicans. Conversely, in states with two Republican senators, 20\% of House seats are held by Democrats. Democratic states do not have large populations of unrepresented Republicans, if any pattern exists, Democrats in Republican states are the ones losing representation by each state having only two senators. 

\section{What might have been}

Then I asked the question, which types of legislation, specifically, are passed or blocked by ``undemocratic votes''? To answer this, I sorted votes by the gap between popular support and Senate votes. Then I examined the top 10 such items that passed the Senate but when senators representing the majority of the U.S. population opposed, giving me the top 10 most undemocratic ``Population no, Senate yes'' votes. I performed the same operation on ``Population yes, Senate no'' votes as well. 

A few comments before the analysis. first, note, as explained above, that Motions to Table complicate interpretations of ``Population yes, Senate no'' and it's counterpart. I did not re-categorize Motions to Table in this segment of the analysis.

Second, also as explained above, recall I could not know for sure amendments were killed permanently and which ones eventually made their way into law at a later date. 

Third and finally, I do not present the entirety of the top 10 lists for each type of ``undemocratic vote''. Votes before 1995 don't have the full text attached to them, so it was difficult for me to understand their purpose. Other votes are wholly procedural and the vote has little bearing on the ultimate fate of the item. The full list is available on replication materials available on Github. 

Among the top 10 ``Population no, Senate yes'' votes are

\begin{itemize}
	\item \href{https://www.congress.gov/amendment/110th-congress/senate-amendment/338}{\emph{Motion to Table Obama Amdt. No. 338}} on March 6th, 2007: An amendment proposed by then-Senator Obama to restrict discretionary spending by the Department of Homeland Security (DHS). The amendment would have ensured that DHS money was allocated based on real risks to national security as advocated by the 9/11 Commission. It was killed despite senators representing 75\% of the population supporting the amendment. 56\% of senators voted in favor of the Motion to Table.  

	\item \href{https://www.congress.gov/amendment/109th-congress/senate-amendment/1142}{\emph{Collins Amdt. No. 1142}} on July 12th, 2005: An amendment which dramatically expanded the budget of the Department of Homeland Security and provide more funding to activities of the DHS in rural areas. The amendment passed with an overwhelming 71 Senate votes, from senators representing only 47\% of the U.S. population.

	\item \href{https://www.congress.gov/amendment/108th-congress/senate-amendment/3631}{\emph{Motion to Table Clinton Amdt. No. 3631}} on September 14th, 2004: Another amendment which would have forced DHS to allocate money based on risks of terrorism advocated by the 9/11 Commission. The motion to table passed, meaning the amendment was killed by 58\% votes in the Senate representing only 35\% of the U.S. population. 

	\item \href{https://www.congress.gov/amendment/101st-congress/senate-amendment/255}{\emph{Motion To Table Point Of Order Re: Amdt. No. 255}} on July 13th, 1989: As an ammendment to the Immigration Act of 1990, Senator Richard Shelby (D-AL) introduced text that sought to

	\begin{quote}
		prevent distortions in the reapportionment of the House of Representatives caused by the use of census population figures which include illegal aliens.
	\end{quote}


	Senator Ted Kennedy challenged this amendment on constitutional grounds and this challenge, the ``Point of Order'' was tabled with 56\% of senators voting yes despite only representing 37\% of the population. Ultimately, the final version of the bill  \href{https://www.nytimes.com/1989/08/03/us/house-rejects-exclusion-of-aliens-in-census.html}{did indeed} count undocumented immigrants in deciding reapportionment.

	\item \href{https://www.congress.gov/amendment/105th-congress/senate-amendment/340}{\emph{Motion to Table Specter Amdt. No. 340}} on May 23rd, 2007: An amendment that would have increased national spending on health research and allocated more funds to discretionay spending. The motion to table passed by 62\% of senators representing 46\% of the population.

	\item \href{https://www.congress.gov/amendment/107th-congress/senate-amendment/3139}{\emph{Motion to Table Boxer Amdt. No. 3139}} on April 25th, 2002: An amendment which would have required ``renewable fuels'' such as ethanol to meet the same liability standards as traditional gasoline. The amendment was killed by 56\% of senators voting, despite only representing 41\% of the population.
\end{itemize}

Among the top 10 ``Population yes, Senate no'' votes are

\begin{itemize}
	\item \href{https://www.congress.gov/amendment/109th-congress/senate-amendment/4634?r=27&s=1}{\emph{Menendez Amdt. No. 4634}} on July 13th, 2006: An amendment similar to the one proposed by Senator Obama, above, which would have ensured DHS funds were targeted towards high-risk areas. 36\% of senators voted for the amendment. It failed despite those representatives representing 67\% of the U.S. population.

	\item \href{https://www.congress.gov/amendment/110th-congress/senate-amendment/335?r=2&s=3}{\emph{Feinstein Amdt. No. 1215, As Modified}}, on July 12th, 2005: A Motion to Table this amendment also appeared on the top 10 ``Senate yes, Population no'' list. It deals, like other amendments listed here, with ensuring that DHS money is allocated based on risk and is not discretionary. 33\% of senators representing 62\% of the U.S. population voted in favor of this amendment.

	\item \href{https://www.congress.gov/amendment/101st-congress/senate-amendment/900}{\emph{Motion To Table Amdt. No. 900}} on September 29th, 1989: This amendment sought to limit funds for used for counting undocumented immigrants. This Motion to Table, consequently, would have defeated the amendment, at least temporarily. The effort to defeat the amendment failed. Only 45\% of senators voted for the Motion to Table, but those senators represented 62\% of the U.S. population. 

	The amendment was ultimately agreed to by a voice vote. Since voice votes are not recorded, we do not know whether or not it was passed by senators representing a minority of the U.S. population. 

	\item Finally, \href{https://www.congress.gov/amendment/103rd-congress/senate-amendment/1632}{\emph{ McCain Amdt. No. 1632}} on April 20th, 1994: The amendment would have democratized the the use of parking spaces designated for Congress. It's stated purpose:

	\begin{quote}
		To express the sense of the Senate that all parking areas reserved at Washington National Airport and Dulles International Airport for Members of Congress and other Government officials should be open for use by the public, and for other purposes.
	\end{quote}

	45\% of the Senate representing 60\%  of the U.S. population voted in favor of the amendment. Congress continues to have access to restricted parking, though their lot is currently closed for construction according to 2017 \href{https://www.nbcwashington.com/news/local/members-of-congress-supreme-court-to-lose-vip-parking-spots-at-dca-for-next-4-years/12517/}{reporting} from DC's local NBC affiliate. Senators will be forced to use the same parking as the rest of the population until 2021.
\end{itemize}

The fact that 5 of the top 20 most ``undemocratic'' votes in the Senate involve the regulation and funding of the Department of Homeland Security is shocking. Anyone familiar with U.S. politics in the mid-2000s is aware of contentious battles in Congress over the Department of Homeland Security. But the true scope of the undemocratic forces leading to the rise of the Department of Homeland security is alarming. Over and over again Democrats representing the majority of the U.S. population attempted to regulate the agency and were defeated by Republican senators representing a minority of voices. 

Could senators have known in the mid 2000s the true urgency of their push to curtail the Department of Homeland Security? As I write this, the Department of Homeland security is acting as President Trump's \href{https://www.opb.org/news/article/portland-oregon-homeland-security-officers-protests-trump-monument-order/}{private} \href{https://www.washingtonpost.com/}{army}. In my hometown, Portland, Oregon, DHS is pulling people off the street into \href{https://www.nytimes.com/2020/07/18/us/portland-protests.html}{unmarked vans}. Every level of government in Oregon has \href{https://www.wweek.com/news/2020/07/16/oregon-gov-kate-brown-says-president-trump-is-invading-portland-as-an-election-stunt/}{called} for the agents to leave the city and as of this writing, they have not complied. My analysis shows the unaccountability of the Department of Homeland Security did not start with President Trump. Rather, the present day lack of control that we as a public have over the agency is mirrored in our institutional structure. 

The fights over the Department of Homeland Security stand out because of their frequency and their relevance to the current moment. But ``undemocratic votes'' occurred on every topic from health care to immigration. Political theorists and self-interested individuals from small states will surely respond to this research by extolling the virtues of temperance and the dangers of a government that bends readily to the wills of a majority. But when is it too much? How many times does the will of the American public need to be overturned before we make Congress a fully democratic institution?

\end{document}
